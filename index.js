// Repetition Control Structures

// While Loop
/* Syntax 
	while(condition){
		statement/s;
	}
*/

/*let count = 5;

// count = 5
while(count !== 0){
	console.log("While: " + count);
	count--;
}

console.log("Display numbers 1-10")
count = 1;

while(count < 11){
	console.log("While: " + count);
	count++;
}
*/
// Do while loop
/* Syntax:
	do{
		statement;
	}while (condition);
*/

// Number() is similar to parseInt when converting String inot numbers
let number = Number(prompt("Give me a number"));

do{
	console.log("Do While: " + number);
	number += 1;
}while (number < 10);

// Create a new variable to be used in displaying even numbers from 2-10 using while loop.

let even = 2;

do{
	console.log("Even: " + even);
	even += 2;
}while (even <= 10);

// For loop
/* Syntax: 
	for(initialixation;condition;stepExpression){
		statement;
	}
*/
console.log("For Loop");

for (let count = 0; count <= 20; count++){
	console.log(count);
}

console.log("Even for loop");
let even1 = 2;
for(let counter = 1; counter <= 5; counter++){
	console.log("Even: " + even1);
	even1 += 2;
}

/*
	counter = 1
	even = 2
*/
/* Expected output:
	Even: 2 
	Even: 4
	Even: 6
	Even: 8
	Even: 10
*/


console.log("Other for loop sanples:")
let myString = 'alex';
// .length property is used to count the characters in a string
console.log(myString.length);

console.log(myString[0]);
console.log(myString[3]);

// myString.length = 4
// x = 0, 1, 2, 3

for (let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}

/*
	for(let x = 3; x >= 0; x--){
		console.log(myString[x]);
	}
*/
/* Expected output:
	a
	l
	e
	x
*/

// Print out letter individually but will print 3 instead of the vowels
let myName = "AlEx"; 

for(let i = 0; i < myName.length; i++){
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
	){
		console.log(3);
	}
	else{
		console.log(myName[i]);
	}
}

// Continue and Break Statements

for (let count = 0; count <= 20; count++){
	// if ramaindre is equal to 0, tells the code to continue to iterate
	if(count % 2 === 0){
		continue;
	}
	console.log("Continue and Break: " + count);

	// if the current value if count is greater than 10, the code to stop the loop

	if(count > 10){
		break;
	}
}

let name = 'alexandro'

/*
	name = 9 
	i = 0
*/

for (let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i] === "d"){
		break;
	}

}